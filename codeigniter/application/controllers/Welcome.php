<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		// Load semua library dan helper yang dibutuhkan
		// Library email digunakan untuk mengirimkan email
		$this->load->library('email');
		// Helper file digunakan untuk read write file txt log pada folder cache
		$this->load->helper('file');
		// Custom helper yg dibuat untuk pemanggilan API
		$this->load->helper('api');

		// Membuat akun email sementara menggunakan Ethereal Email dan simpan pada variable, fungsi ini diambil dari nodemailer untuk nodejs (https://github.com/nodemailer/nodemailer)
		$data = array('requestor' => 'nodemailer', 'version' => '6.3.0');
		$hasilAPI = CallAPI('POST', 'https://api.nodemailer.com/user', $data);
		$objAPI = json_decode($hasilAPI);
		$mail = $objAPI->user;
		$pass = $objAPI->pass;
		
		// SMTP config
		$config['protocol'] = 'smtp';
		$config['smtp_crypto'] = 'tls';
		$config['smtp_host'] = $objAPI->smtp->host;
		$config['smtp_port'] = $objAPI->smtp->port;
		$config['smtp_user'] = $mail;
		$config['smtp_pass'] = $pass;
		
		// Kirim email
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from($mail);
		$this->email->to($mail);
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		$mailStat = $this->email->send();

		// Cek apakah email berhasil terkirim
		if ($mailStat === true) {
			echo 'Email sent!';
		} else {
			echo 'Send mail failed!';
		}
		
		// Buka log dan tampilkan pada browser
		$file_path = FCPATH . '/application/cache/writeme.txt';
		$string = file_get_contents($file_path);
		echo 'Email Log: <br/>';
		echo '<pre>'.$string.'</pre>';
	}
}
