<?php
// Extending class dari Email Class native codeigniter
class MY_Email extends CI_Email {
	public function send($auto_clear = TRUE){
		// Dapatkan email pengirim, ini bisa didapatkan melalui fungsi set_header pada parent Class
		$kepala = parent::set_header($header = '', $value = '');
		// Dapatkan status pengiriman email apakah berhasil atau tidak
		$hasil = parent::send($auto_clear = TRUE);
		// Cek status pengiriman email, jika berhasil maka tulis log pada file txt di folder cache
		if ($hasil === TRUE) {
			$mail = 'DUMMY';
			$file_path = FCPATH . '/application/cache/writeme.txt';
			$data = $kepala->smtp_user." has just sent an email";
			if ( file_exists($file_path) ) {
				write_file( $file_path, $data . PHP_EOL, 'a' );
			}else{
				write_file( $file_path, $data . PHP_EOL );
			}
		}
	}
}
