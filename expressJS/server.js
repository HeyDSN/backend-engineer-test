require('dotenv').config()
const appHttp = require('./app')
const debug = require('debug')
const http = require('http')
const portHttp = parseInt(process.env.PORT, 10)
appHttp.set('port', portHttp)
const serverHttp = http.createServer(appHttp)

serverHttp.listen(portHttp)

serverHttp.on('error', (error) => {
	if (error.syscall !== 'listen') {
		throw error
	}
	const bindHttp = typeof portHttp === 'string' ?
		'Pipe ' + portHttp :
		'Port ' + portHttp
	switch (error.code) {
		case 'EACCES':
			debug(bindHttp + ' requires elevated privileges')
			process.exit(1)
			break
		case 'EADDRINUSE':
			debug(bindHttp + ' is already in use')
			process.exit(1)
			break
		default:
			throw error
	}
})
serverHttp.on('listening', () => {
	const addrHttp = serverHttp.address()
	const bindHttp = typeof addrHttp === 'string' ?
		'pipe ' + addrHttp :
		'port ' + addrHttp.port
	debug('Listening on ' + bindHttp)
	console.log('Listening on ' + portHttp)
})
