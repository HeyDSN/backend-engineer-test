require('dotenv').config()
process.env.NODE_ENV = 'test'
// Test module
const chai = require('chai')
chai.use(require('chai-http'))
const expect = require('chai').expect
// Main function
const api = require('../app.js')
// Test app
describe('API Test', function () {
	it('should search with param', function (done) {
		chai.request(api)
			.get('/search/?keyword=Batman&page=2')
			.then(function (res) {
				try {
					expect(res).to.have.status(200)
					expect(res).to.be.json
					expect(res.body).to.be.an('object')
					expect(res.body.data.Response).to.be.equal('True')
					done()
				} catch (err) {
					done(err)
				}
			})
	})
	it('should error without param', function (done) {
		chai.request(api)
			.get('/search')
			.then(function (res) {
				try {
					expect(res).to.have.status(200)
					expect(res).to.be.json
					expect(res.body).to.be.an('object')
					expect(res.body.data.Response).to.be.equal('False')
					done()
				} catch (err) {
					done(err)
				}
			})
	})
	it('should return 404', function (done) {
		chai.request(api)
			.get('/404')
			.then(function (res) {
				try {
					expect(res).to.have.status(404)
					expect(res).to.be.json
					expect(res.body).to.be.an('object')
					done()
				} catch (err) {
					done(err)
				}
			})
	})
})
