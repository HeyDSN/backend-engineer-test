const request = require('request')
const express = require('express')
const router = express.Router()
router.get('/search', (req, res) => {

	let url = process.env.API_URL
	let qry = {
		apikey: process.env.API_KEY,
		s: req.query.keyword || '',
		page: req.query.id || 1
	}

	request({ url: url, qs: qry }, function(err, response, body) {
		/* istanbul ignore if */
		if (err) {
			return res.status(500).json({
				code: 500,
				message: "Internal Server Error"
			})
		}
		let bodyJSON = JSON.parse(body)
		res.status(response.statusCode).json({
			code: response.statusCode,
			message: response.statusMessage,
			data: bodyJSON
		})
	})

})
module.exports = router
