<?php
echo 'Soal 2a, mengambil string yang berada dalam tanda kurung<br/><br/>';
// Text
$text = '(23 Maret 2018) Some string is below here (please note below):
The dummy formula is a=(x+y)-100.';

// Filter text dan simpan hasil setiap string yang berada pada dalam kurung kedalam array dengan fungsi preg_match_all
preg_match_all('#\((.*?)\)#', $text, $filtered);
// Looping array yang telah didapatkan dari pregmatch, array[0] merupakan value asli dari text, dan array[1] adalah hasil filter dan merupakan multidimensional yang berisikan string-string hasil filter
foreach($filtered[1] as $value){
	// Tampilkan string
	echo $value.' ';
}
?>
