<?php
echo 'Soal 1a, function menampilkan value dari param<br/><br/>';

/**
 * Berbagai contoh value dari variable param yang nanti akan dikirim ke fungsi (uncomment salah satu variable param)
 */

// $param = 'merupakan string'; //string
// $param = 1743; //integer
// $param = [1, 2, 3]; //array one dimensional
// $param = [1, 2, [1, 2, [6, [4, [99], 1], 6], 0], 3]; //array multi dimensional

// Panggil fungsi dengan parameter
$valueGet = tampil_param($param);

// Tampilkan hasil dari proses fungsi
echo $valueGet;

/**
 * The Function
 */
function tampil_param($param) {
	//buat varialbe kosong untuk menyimpan hasil dari value nantinya
	$str = '';
	// Dapatkan type data dari param
	$type = gettype($param);
	// Cek type data dari value parameter
	if ($type !== 'array') {
		$str .= $param.'<br/>';
	} else {
		// Jika type data adalah array, maka dilakukan looping untuk memcah isi array untuk kemudia disimpan pada variable str
		foreach ($param as $arr) {
			// Dapatkan type data dari value array
			$typeArr = gettype($arr);
			// Cek type data dari isi pada array untuk mengetahui dimensi array, jika multi dimensional akan dilakukan recrusive check dengan fungsi yang sama yaitu { tampil_param() }
			if ($type !== 'array') {
				// Jika type data bukan array, maka tampilkan data langsung disimpan pada variable str
				$str .= $arr.'<br/>';
			}else {
				// Jika type data array lakukan pengecekan ulang untuk mengetahui array tersebut merupakan multi dimensional array atau bukan
				$str .= tampil_param($arr);
			}
		}
	}
	// Setelah proses selesai kembalikan value str
	return $str;
}
?>
